package com.androidapps.rolandok.firstproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = MainActivity.class.getSimpleName();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Toast.makeText(this, "onCreate()", Toast.LENGTH_SHORT).show();
    Log.d(MainActivity.TAG, "In-OnCreate()");
  }

  @Override
  protected void onStart() {
    super.onStart();

    Toast.makeText(this, "onStart()", Toast.LENGTH_SHORT).show();
    Log.d(MainActivity.TAG, "In-OnStart()");
  }

  @Override
  protected void onResume() {
    super.onResume();

    Toast.makeText(this, "onResume()", Toast.LENGTH_SHORT).show();
    Log.e(MainActivity.TAG, "In-onResume()");
  }

  @Override
  protected void onPause() {
    super.onPause();

    Toast.makeText(this, "onPause()", Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onStop() {
    super.onStop();

    Toast.makeText(this, "onStop()", Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    Toast.makeText(this, "onDestroy()", Toast.LENGTH_SHORT).show();

  }

  @Override
  protected void onRestart() {
    super.onRestart();

  }

  public void startSecondActivity(View view) {
    Intent activityIntent = new Intent(this, SecondActivity.class);
    startActivity(activityIntent);
  }
}
